from direction import Direction

class Dir_Green(Direction):

    def __init__(self, car):

        super().__init__(car)
        self.location = car.location
        self.facing = car.facing
        self.mass = 15
        self.velocity = [4, 0]
        self.max_force = 2
        self.max_speed = 5
        self.min_speed = 1
        self.building = [350, 400, 20]
        self.distance = 130


    def get_away(self, x, y):
        away_from = [self.location[0] - x, self.location[1] - y]
        away = Direction.vector_multi(Direction.vector_div(away_from, Direction.length(away_from)),
                                  self.min_speed)
        return away
