from PyQt5 import QtWidgets, QtGui, QtCore


class CarGraphic(QtWidgets.QGraphicsPolygonItem):

    def __init__(self, car):

        super(CarGraphic, self).__init__()
        self.car = car

        red = QtGui.QColor("red")
        yellow = QtGui.QColor("yellow")
        green = QtGui.QColor("green")
        blue = QtGui.QColor("cyan")

        if car.get_color() == "red":
            brush = QtGui.QBrush(red)
        elif car.get_color() == "green":
            brush = QtGui.QBrush(green)
        elif car.get_color() == "yellow":
            brush = QtGui.QBrush(yellow)
        else:
            brush = QtGui.QBrush(blue)

        self.setBrush(brush)
        self.construct()
        self.update_location()
        self.update_facing()


    def construct(self):
        car = QtGui.QPolygonF()

        car.append(QtCore.QPointF(0, 13))
        car.append(QtCore.QPointF(8, 13))
        car.append(QtCore.QPointF(8, 0))
        car.append(QtCore.QPointF(0, 0))


        self.setPolygon(car)

        self.setTransformOriginPoint(15 / 2, 15 / 2)

    def update_location(self):
        loc = self.car.location
        self.setPos(loc[0], loc[1])

    def update_facing(self):
        fac = self.car.facing
        self.setRotation(fac)

    def mousePressEvent(self, *args, **kwargs):

        if self.car.moving == False:
            self.car.moving = True
        else:
            self.car.moving = False




