from PyQt5 import QtWidgets, QtCore
from car_graphic import CarGraphic
from building_graphic import BuildingGraphic
from phantom_graphic import PhantomGraphic
from direction import Direction
from car import Car
from  dir_red import Dir_red
from dir_blue import Dir_Blue
from dir_yellow import Dir_Yellow
from dir_green import Dir_Green


class GUI(QtWidgets.QMainWindow):

    def __init__(self, city):
        super().__init__()
        self.setCentralWidget(QtWidgets.QWidget())
        self.vertical = QtWidgets.QVBoxLayout()
        self.centralWidget().setLayout(self.vertical)
        self.init_window()
        self.city = city
        self.cars = []
        self.buildings =[]
        self.phantom = []
        self.add_buttons()

        self.add_buildings(city)
        self.add_phantom_buildings(city)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update_cars)
        self.timer.start(10)

        self.timer2 = QtCore.QTimer()
        self.timer2.timeout.connect(self.update)
        self.timer2.start(40)


    def init_window(self):
        self.setGeometry(300, 300, 800, 800)
        self.setWindowTitle('City')
        self.show()

        self.scene = QtWidgets.QGraphicsScene()
        self.scene.setSceneRect(0, 0, 700, 630)

        self.view = QtWidgets.QGraphicsView(self.scene, self)
        self.view.adjustSize()
        self.view.show()
        self.vertical.addWidget(self.view)


    def add_red_car(self):
        car = Car("red")
        dir = Dir_red(car)
        car.set_direction(dir)
        if self.city.add_car(car):
            self.add_car_graphic(car)

    def add_blue_car(self):
        car = Car("blue")
        dir = Dir_Blue(car)
        car.set_direction(dir)
        if self.city.add_car(car):
            self.add_car_graphic(car)

    def add_yellow_car(self):
        car = Car("yellow")
        dir = Dir_Yellow(car)
        car.set_direction(dir)
        if self.city.add_car(car):
            self.add_car_graphic(car)

    def add_green_car(self):
        car = Car("green")
        dir = Dir_Green(car)
        car.set_direction(dir)
        if self.city.add_car(car):
            self.add_car_graphic(car)


    def add_car_graphic(self, car):
        new = CarGraphic(car)
        self.scene.addItem(new)
        self.cars.append(new)


    def update_cars(self):
        for cargraphic in self.cars:
            for pbuilding in self.phantom:
                if cargraphic.collidesWithItem(pbuilding):


                    steer = Direction.vector_dif(cargraphic.car.direction.get_away(pbuilding.x, pbuilding.y), cargraphic.car.direction.velocity)

                    cargraphic.car.direction.velocity = Direction.vector_multi(Direction.vector_div(cargraphic.car.direction.velocity, Direction.length(cargraphic.car.direction.velocity)),
                                                                               (cargraphic.car.direction.min_speed))

                    if Direction.length(steer) > cargraphic.car.direction.max_force:
                        steer_force = Direction.vector_multi(Direction.vector_div(steer, Direction.length(steer)), cargraphic.car.direction.max_force)

                    else:
                        steer_force = steer

                    acceleration = Direction.vector_div(steer_force, cargraphic.car.direction.mass)

                    if Direction.length(Direction.vector_sum(cargraphic.car.direction.velocity, acceleration)) > cargraphic.car.direction.max_speed:

                        move = Direction.vector_sum(cargraphic.car.direction.velocity, acceleration)
                        cargraphic.car.direction.velocity = Direction.vector_multi(Direction.vector_div(move, Direction.length(move)), cargraphic.car.direction.max_speed)

                    else:
                        cargraphic.car.direction.velocity = Direction.vector_sum(cargraphic.car.direction.velocity, acceleration)

            cargraphic.update_location()
            cargraphic.update_facing()


    def update(self):
        self.update_cars()
        self.city.next_full_turn()



    def add_buttons(self):
        self.red_btn = QtWidgets.QPushButton("Add red car")
        self.red_btn.clicked.connect(self.add_red_car)
        self.vertical.addWidget(self.red_btn)

        self.blue_btn = QtWidgets.QPushButton("Add blue car")
        self.blue_btn.clicked.connect(self.add_blue_car)
        self.vertical.addWidget(self.blue_btn)

        self.yellow_btn = QtWidgets.QPushButton("Add yellow car")
        self.yellow_btn.clicked.connect(self.add_yellow_car)
        self.vertical.addWidget(self.yellow_btn)

        self.green_btn = QtWidgets.QPushButton("Add green car")
        self.green_btn.clicked.connect(self.add_green_car)
        self.vertical.addWidget(self.green_btn)

    def add_buildings(self, city):
        for building in city.get_buildings():
            new = BuildingGraphic(building[0], building[1], building[2])
            self.scene.addItem(new)
            self.buildings.append(new)

    def add_phantom_buildings(self, city):
        for pbuilding in city.get_phantom_buildings():
            new = PhantomGraphic(pbuilding[0], pbuilding[1], pbuilding[2], pbuilding[3])
            self.scene.addItem(new)
            self.phantom.append(new)



