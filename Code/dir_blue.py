from direction import Direction

class Dir_Blue(Direction):

    def __init__(self, car):

        super().__init__(car)
        self.location = car.location
        self.facing = car.facing
        self.mass = 12
        self.velocity = [-3, 0]
        self.max_force = 3
        self.max_speed = 5
        self.min_speed = 2.2
        self.building = [450, 100, 25]
        self.distance = 100


    def get_away(self, x, y):

        away_from = [self.location[0] - x, self.location[1] - y]
        away = Direction.vector_multi(Direction.vector_div(away_from, Direction.length(away_from)),
                                         self.min_speed)
        return away