from city import City
import sys
from PyQt5.QtWidgets import QApplication
from gui import GUI


def main():
    city = City()


    global app
    app = QApplication(sys.argv)
    gui = GUI(city)

    sys.exit(app.exec_())



main()