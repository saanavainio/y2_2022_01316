from direction import Direction

class Dir_red(Direction):

    def __init__(self, car):

        super().__init__(car)
        self.location = car.location
        self.facing = car.facing
        self.mass = 8
        self.velocity = [4, 0]
        self.max_force = 3
        self.max_speed = 6
        self.min_speed = 3
        self.building = [70, 70, 100]
        self.distance = 250

    def get_away(self, x, y):

        away_from = [(self.location[0]) - x, (self.location[1] - y)]
        away = Direction.vector_multi(Direction.vector_div(away_from, Direction.length(away_from)),
                                         self.min_speed)
        return away
