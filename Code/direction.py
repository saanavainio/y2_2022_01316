
import math

class Direction():

    def __init__(self, car):
        self.car = car
        self.location = car.location
        self.mass = None
        self.velocity = None
        self.max_force = None
        self.max_speed = None
        self.min_speed = None
        self.facing = car.facing
        self.building = None
        self.distance = None

    @staticmethod
    def length(list):
        length = math.sqrt((list[0] ** 2) + (list[1] ** 2))
        return length

    @staticmethod
    def vector_sum(list1, list2):
        sum = [list1[0]+list2[0], list1[1]+list2[1]]
        return sum

    @staticmethod
    def vector_multi(list, m):
        multi = [list[0] * m, list[1] * m]
        return multi

    @staticmethod
    def vector_div(list, d):
        div = [list[0] / d, list[1] /d]
        return div

    @staticmethod
    def vector_dif(list1, list2):
        dif = [list1[0]-list2[0], list1[1]-list2[1]]
        return dif



    def drive(self):


        speed = self.length(self.velocity)


        future = self.vector_sum(self.location, self.vector_multi(self.vector_div(self.velocity,speed), 20))
        dist_building = math.dist([self.location[0], self.location[1]], [self.building[0], self.building[1]])

        if self.car.moving == True:

            if self.car.get_city().contains(future[0], future[1]) == False:
                self.velocity_out_of_bounds(speed)

            elif dist_building < self.distance:
                self.velocity_small_distance(speed)

            elif dist_building > self.distance:

                self.velocity_large_distance(speed)

            else:
                self.velocity_right_distance(speed, future)




            self.location = self.vector_sum(self.location, self.velocity)

            self.car.update_location(self.location)

            facing = self.get_facing2(self.velocity)  # changing the facing direction
            self.facing = facing
            self.car.update_facing(facing)



    def velocity_out_of_bounds(self, speed):

        to_center = [250 - self.location[0], 250 - self.location[1]] # steering the car away from the wall
        desired = self.vector_multi(self.vector_div(to_center, self.length(to_center)), self.max_speed)

        steer = self.vector_dif(desired, self.velocity)

        self.velocity = self.vector_multi(self.vector_div(self.velocity, self.length(self.velocity)), (speed - 2))


        if self.length(steer) > self.max_force:
            steer_force = self.vector_multi(self.vector_div(steer, self.length(steer)), self.max_force)

        else:
            steer_force = steer

        acceleration = self.vector_div(steer_force, self.mass)


        move = self.vector_sum(self.velocity, acceleration)
        self.velocity = self.vector_multi(self.vector_div(move, self.length(move)), self.min_speed)




    def velocity_small_distance(self, speed):

        out_of_building = [self.location[0] - self.building[0], self.location[1] - self.building[1]]
        desired = self.vector_multi(self.vector_div(out_of_building, self.length(out_of_building)), speed)

        steer = self.vector_dif(desired, self.velocity)

        if self.length(steer) > self.max_force:
            steer_force = self.vector_multi(self.vector_div(steer, self.length(steer)), self.max_force)

        else:
            steer_force = steer

        acceleration = self.vector_div(steer_force, self.mass)

        if self.length(self.vector_sum(self.velocity, acceleration)) > self.max_speed:
            move = self.vector_sum(self.velocity, acceleration)
            pos_velocity = self.vector_multi(self.vector_div(move, self.length(move)), self.max_speed)

        else:
            pos_velocity = self.vector_sum(self.velocity, acceleration)

        if self.length(pos_velocity) < self.min_speed:
            pos_velocity = self.vector_multi(self.vector_div(pos_velocity, self.length(pos_velocity)), self.min_speed)

        pos_future = self.vector_sum(self.location, self.vector_multi(self.vector_div(pos_velocity, self.length(pos_velocity)), 20))



        for car in self.car.get_city().get_cars():

            if car != self.car:
                distance = self.length(self.vector_dif(pos_future, car.get_location()))

                if distance < 25:

                    self.velocity = self.vector_multi(self.vector_div(self.velocity, self.length(self.velocity)),
                                                      (speed - 2))

                    steer_away_from_car = self.vector_dif(self.location, car.get_location())

                    if self.length(steer_away_from_car) > self.max_force:
                        steer_force = self.vector_multi(self.vector_div(steer_away_from_car, self.length(steer_away_from_car)), self.max_force)
                    else:
                        steer_force = steer_away_from_car

                    acceleration2 = self.vector_div(steer_force, self.mass + 2)


                    move = self.vector_sum(self.velocity, acceleration2)
                    pos_velocity = self.vector_multi(self.vector_div(move, self.length(move)), 1)

                    break



        self.velocity = pos_velocity


    def velocity_large_distance(self, speed):

        closer_to_building = [(self.building[0] - self.location[0]), (self.building[1] - self.location[1])]
        desired = self.vector_multi(self.vector_div(closer_to_building, self.length(closer_to_building)), speed)
        steer = self.vector_dif(desired, self.velocity)

        if self.length(steer) > self.max_force:
            steer_force = self.vector_multi(self.vector_div(steer, self.length(steer)), self.max_force)
        else:
            steer_force = steer

        acceleration = self.vector_div(steer_force, self.mass)

        if self.length(self.vector_sum(self.velocity, acceleration)) > self.max_speed:
            move = self.vector_sum(self.velocity, acceleration)
            pos_velocity = self.vector_multi(self.vector_div(move, self.length(move)), self.max_speed)
        else:
            pos_velocity = self.vector_sum(self.velocity, acceleration)

        if self.length(pos_velocity) < self.min_speed:
            pos_velocity = self.vector_multi(self.vector_div(pos_velocity, self.length(pos_velocity)), self.min_speed)

        pos_future = self.vector_sum(self.location, self.vector_multi(self.vector_div(pos_velocity, self.length(pos_velocity)), 20))

        for car in self.car.get_city().get_cars():

            if car != self.car:
                distance = self.length(self.vector_dif(pos_future, car.get_location()))

                if distance < 25:

                    self.velocity = self.vector_multi(self.vector_div(self.velocity, self.length(self.velocity)),
                                                      (speed - 2))

                    steer_away_from_car = self.vector_dif(self.location, car.get_location())

                    if self.length(steer_away_from_car) > self.max_force:
                        steer_force = self.vector_multi(self.vector_div(steer_away_from_car, self.length(steer_away_from_car)), self.max_force)
                    else:
                        steer_force = steer_away_from_car

                    acceleration2 = self.vector_div(steer_force, self.mass + 2)


                    move = self.vector_sum(self.velocity, acceleration2)
                    pos_velocity = self.vector_multi(self.vector_div(move, self.length(move)), 1)


                    break



        self.velocity = pos_velocity





    def velocity_right_distance(self, speed, future):

        for car in self.car.get_city().get_cars():

            if car != self.car:
                distance = self.length(self.vector_dif(future, car.get_location()))

                if distance < 25:

                    self.velocity = self.vector_multi(self.vector_div(self.velocity, self.length(self.velocity)),
                                                      (speed - 2))
                    steer_away_from_car = self.vector_dif(self.location, car.get_location())

                    if self.length(steer_away_from_car) > self.max_force:
                        steer_force = self.vector_multi(self.vector_div(steer_away_from_car, self.length(steer_away_from_car)), self.max_force)
                    else:
                        steer_force = steer_away_from_car

                    acceleration2 = self.vector_div(steer_force, self.mass + 2)


                    move = self.vector_sum(self.velocity, acceleration2)
                    pos_velocity = self.vector_multi(self.vector_div(move, self.length(move)), 1)


                    self.velocity = pos_velocity

                    break




    @staticmethod
    def get_facing2(vector):    # getting the rotation of the car

        if vector[0] == 0 and vector[1] > 0:
            facing = 0
        elif vector[0] == 0 and vector[1] < 0:
            facing = 180
        elif vector[0] > 0 and vector[1] == 0:
            facing = 360-90
        elif vector[0] < 0 and vector[1] == 0:
            facing = 360-270

        else:

            angle_rad = (math.atan(vector[1]/vector[0]))
            angle = math.degrees(angle_rad)

            if (vector[0] > 0) and (vector[1] > 0):
                facing = 360- (90 - angle)

            elif (vector[0] > 0) and (vector[1] < 0):
                facing = 360-(90 - angle)

            elif (vector[0] < 0) and (vector[1] < 0):
                facing = 360-(270 - angle)

            else:
                facing = 360-(270 - angle)

        return facing

