from PyQt5 import QtWidgets, QtGui

class PhantomGraphic(QtWidgets.QGraphicsEllipseItem):
    def __init__(self,x,y,r,ph):

        super(PhantomGraphic, self).__init__()
        self.x = x
        self.y = y
        self.r = r
        self.pr = ph


        magenta = QtGui.QColor("magenta")
        magenta.setAlpha(0)
        brush1 = QtGui.QBrush(magenta)
        pen = QtGui.QPen(magenta)


        self.setBrush(brush1)
        self.setPen(pen)
        self.construct()
        self.set_location()


    def construct(self):

        self.setRect((self.r) / 2, (self.r) / 2, self.r, self.r)
        self.setTransformOriginPoint(self.r / 2, self.r / 2)


    def set_location(self):

        self.setPos((self.x - (self.r-self.pr)), self.y - (self.r-self.pr))