import unittest

from city import City
from car import Car
from dir_red import Dir_red
from dir_blue import Dir_Blue
from dir_yellow import Dir_Yellow
from dir_green import Dir_Green
import math


class Test(unittest.TestCase):

    def setUp(self):

        self.test_city = City()

        car1 = Car("red")
        dir1 = Dir_red(car1)
        car1.set_direction(dir1)
        self.test_city.add_car(car1)

        car2 = Car("blue")
        dir2 = Dir_Blue(car2)
        car2.set_direction(dir2)
        self.test_city.add_car(car2)

        car3 = Car("yellow")
        dir3 = Dir_Yellow(car3)
        car3.set_direction(dir3)
        self.test_city.add_car(car3)

        car4 = Car("green")
        dir4 = Dir_Green(car4)
        car4.set_direction(dir4)
        self.test_city.add_car(car4)





    def test_cars(self): #testing the distance of the cars from the borders and buildings

        for i in range(0,100000):
            self.test_city.next_full_turn()

        for car in self.test_city.get_cars():
            self.assertEqual(True, self.test_city.contains(car.location[0], car.location[1]))

        for car in self.test_city.get_cars():
            for building in self.test_city.get_buildings():
                distance = math.dist([building[0], building[1]], [car.location[0], car.location[1]])
                self.assertGreaterEqual(distance, building[2])

        for car in self.test_city.get_cars():
            for other in self.test_city.get_cars():
                if car != other:
                    distance = math.dist([other.location[0], other.location[1]], [car.location[0], car.location[1]])
                    self.assertGreaterEqual(distance, 20)





if __name__ == "__main__":
    unittest.main()