
class City():


    def __init__(self):
        self.cars = []
        self.buildings = []
        self.turn = 0
        self.phantom_buildings = []

        self.add_building(350, 400, 80)
        self.add_building(450, 90, 60)
        self.add_building(65, 55, 100)
        self.add_building(-500, 300, 500)

        self.add_phantom_building(450, 100, 200, 60)
        self.add_phantom_building(350, 400, 280, 80)
        self.add_phantom_building(70, 70, 280, 100)
        self.add_phantom_building(-500, 300, 700, 500)


    def get_cars(self):
        return self.cars

    def get_buildings(self):
        return self.buildings

    def get_phantom_buildings(self):
        return self.phantom_buildings


    def add_car(self, car):
        if car.set_city(self):
            if self.get_number_of_cars() < 10 and self.get_number_of_cars_color(car.color) < 3:
                self.cars.append(car)
                return True
            else:
                return False
        else:
            return False

    def add_building(self, x, y, r):
        self.buildings.append([x,y,r])

    def add_phantom_building(self, x, y, r, ph):
        self.phantom_buildings.append([x,y,r, ph])



    def get_number_of_cars(self):
        return len(self.cars)

    def get_number_of_cars_color(self, color):
        num = 0
        for car in self.cars:
            if car.color == color:
                num += 1

        return num

    def get_next_car(self):
        if self.get_number_of_cars() < 1:
            return None
        else:
            return self.cars[self.turn]

    def next_car_drive(self):
        current = self.get_next_car()
        if current is not None:
            self.turn = (self.turn + 1) % self.get_number_of_cars()
            current.drive_turn()

    def next_full_turn(self):
        for i in range(self.get_number_of_cars()):
            self.next_car_drive()


    def contains(self, x, y):
        return 0 <= x <= 700 and 0 <= y <= 600




