
import math

class Car():

    def __init__(self, color):
        self.city = None
        self.color = color
        self.set_location(color)
        self.direction = None
        self.moving = True
        self.set_facing(color)


    def set_location(self, color):
        if color == "red":
            self.location = [-50, 50]
        elif color == "yellow":
            self.location = [720, 600]
        elif color == "blue":
            self.location = [400, -30]
        elif color == "green":
            self.location = [350, 0]

    def set_facing(self, color):
        if color == "red":
            self.facing = 90
        elif color == "yellow":
            self.facing = 0
        elif color == "blue":
            self.facing = 180
        elif color == "green":
            self.facing = 180

    def get_direction(self):
        return self.direction

    def get_location(self):
        return self.location

    def get_color(self):
        return self.color

    def is_moving(self):
        return self.moving


    def set_city(self, city):
        a = self.location[0]
        b = self.location[1]
        for car in city.get_cars():
            x = car.location[0]
            y = car.location[1]
            distance = math.sqrt((a - x) ** 2 + (b - y) ** 2)
            if distance < 25: # limit for placing new cars near other cars
                return False
        self.city = city
        return True


    def drive_turn(self):
        if self.is_moving():
            self.direction.drive()

    def get_city(self):
        return self.city

    def update_location(self, new_location):
        self.location = new_location

    def update_facing(self, new_facing):
        self.facing = new_facing

    def set_direction(self, direction):
        self.direction = direction




