from PyQt5 import QtWidgets, QtGui

class BuildingGraphic(QtWidgets.QGraphicsEllipseItem):
    def __init__(self,x,y,r):

        super(BuildingGraphic, self).__init__()
        self.x = x
        self.y = y
        self.r = r


        gray = QtGui.QColor("gray")
        brush = QtGui.QBrush(gray)

        self.setBrush(brush)
        self.construct()
        self.set_location()


    def construct(self):

        self.setRect(self.r / 2, self.r / 2, self.r, self.r)
        self.setTransformOriginPoint(self.r / 2, self.r / 2)


    def set_location(self):

        self.setPos(self.x, self.y)