# Y2_2022_01316

# Liikennesimulaatio

## Lopullinen palautus Liikennesimulaatio-projektista, jossa käyttäjä voi lisätä autoja (3 jokaista väriä, 10 yhteensä) ja pysäyttää niitä hiirellä klikkaamalla.

## Tiedosto- ja kansiorakenne

Code-kansiosta löytyy kaikki ohjelman kooditiedostot, mukaanlukien main-funktio, jolla ohjelma käynnistetään. Kaikki koodi on itsetehtyä. Documents-kansiosta löytyy dokumentaatio ja suunnitelma. 

## Asennusohje

Ohjelma tarvitsee vain PyQt5-kirjaston ja pythonin sisäisen math-luokan. 

## Käyttöohje

Ohjelman voi ajaa ajamalla main.py tiedoston main-funktion. Komentoriviltä ei anneta komentoja, eikä ohjelma käytä erillisiä tiedostoja. Käyttäjä voi painaa ikkunan nappeja lisätäkseen eri värisiä autoja ja klikata autoja pysäyttääkseen ne, ja klikata niitä uudestaan, jolloin ne alkavat liikkua uudestaan.

